import com.mongodb.*;


import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by STEV on 19-01-2015.
 */

//Se solicita una querie que elimine la peor nota de cada estudiante para el tipo 'homework'
public class W2_Tarea2 {

    public static void main(String[] args) throws UnknownHostException {

        MongoClient client = new MongoClient();
        DB db= client.getDB("students");
        DBCollection colection= db.getCollection("grades");

        QueryBuilder query= QueryBuilder.start("type").is("homework");

        DBCursor cursor= colection.find(query.get()).sort(new BasicDBObject("student_id",1).append("score",1));

        List lista_distintos= colection.distinct("student_id");
        int i=0;

        try {
            while (cursor.hasNext() && i< lista_distintos.size() ) {
                DBObject grade = cursor.next();

                if(grade.get("student_id").equals(i)) {
                    colection.remove(grade);
                    System.out.println(grade);
                    i++;
                }
            }
        }finally {
            cursor.close();
        }


    }
}
