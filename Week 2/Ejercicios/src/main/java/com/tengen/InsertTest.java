/*
 * Copyright (c) 2008 - 2013 10gen, Inc. <http://10gen.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tengen;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import org.bson.types.ObjectId;

import java.net.UnknownHostException;
import java.util.Arrays;

public class InsertTest {
    public static void main(String[] args) throws UnknownHostException {
        MongoClient client = new MongoClient();
        DB courseDB = client.getDB("course");
        DBCollection collection = courseDB.getCollection("insertTest");

        collection.drop();

        //Si instancio un BasicDBObject sin _id, genera uno para el documento cuando lo ingresa
        DBObject doc = new BasicDBObject().append("x", 1);
        System.out.println("DOC1, instanciado sin _id: "+doc);
        collection.insert(doc);
        System.out.println("DOC1, luego de insertar: "+doc); //Ahora doc tiene un campo para _id


        //Si instancio generando el _id, obtengo el mismo _id antes y despues de insertar.
        DBObject doc2= new BasicDBObject("_id", new ObjectId()).append("x",2);

        System.out.println("DOC2, instanciado con _id: "+doc2);
        collection.insert(doc2);
        System.out.println("DOC2, luego de insertar: "+doc2); //Mismo _id en ambos casos

        
        //Se puede insertar como lista con Arrays.asList()
        DBObject doc3 = new BasicDBObject().append("x", 3);
        DBObject doc4 = new BasicDBObject().append("x", 4);
        collection.insert(Arrays.asList(doc3, doc4));
       
        //Se obtendrá un error si ingreso un documento que ya inserte antes porque la _id ya existe en el servidor,
        //a menos que borre la id

        System.out.println("DOC3 con su _id antigua "+doc3);
        doc3.removeField("_id");  //Si borro la _id
        collection.insert(doc3);
        System.out.println("DOC3 fue borrado su _id antigua "+doc3);


    }
}
