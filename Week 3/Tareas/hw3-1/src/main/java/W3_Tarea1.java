import com.mongodb.*;


import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by STEV on 19-01-2015.
 */

//Se solicita una querie que elimine la peor nota de cada estudiante para el tipo 'homework'
public class W3_Tarea1 {

    public static void main(String[] args) throws UnknownHostException {

        MongoClient client = new MongoClient();
        DB db= client.getDB("school");
        DBCollection colection= db.getCollection("students");

        QueryBuilder query= QueryBuilder.start("scores.type").is("homework");

        DBCursor cursor= colection.find(query.get() ). //,new BasicDBObject("scores", true)).
                sort(new BasicDBObject("student_id", 1));

        try{

        while(cursor.hasNext()){

            DBObject student= cursor.next();
           List<DBObject> scores = (List<DBObject>) student.get("scores");

            int i=0;
            boolean primero= true;
            List<DBObject> scores_update= scores;
            BasicDBObject temp= new BasicDBObject();
            Float min= Float.valueOf(0);

            while(i< scores.size()) {

               if (scores.get(i).get("type").equals("homework")) {
                    //System.out.println(student.get("_id")+ " "+ scores.get(i));

                   if(primero ) {
                       min= Float.parseFloat(scores.get(i).get("score").toString());
                       temp= (BasicDBObject) scores.get(i);
                       primero=false;
                       System.out.println("Primer min "+student.get("_id")+" "+ min+ " "+temp);
                   }
                   if(!primero && min > Float.parseFloat(scores.get(i).get("score").toString())){
                       min= Float.parseFloat(scores.get(i).get("score").toString());
                       temp= (BasicDBObject) scores.get(i);
                       System.out.println("Mejor min "+student.get("_id")+" "+ min+ " "+temp);
                   }
                }
                i++;
            }
            System.out.println("Salida " + student.get("_id") + " " + min + " " + temp);
            scores.remove(temp);

            //Actualizo en servidor Modo 1
            colection.update(new BasicDBObject("_id",student.get("_id")), new BasicDBObject("$set", new BasicDBObject("scores",scores)));

            //Actualizo en servidor Modo 2
            //student.put("scores",scores);
            //colection.update(new BasicDBObject("_id",student.get("_id")), student);



        }} finally{
            cursor.close();
        }
        /*List lista_distintos= colection.distinct("student_id");
        int i=0;

        try {
            while (cursor.hasNext() && i< lista_distintos.size() ) {
                DBObject grade = cursor.next();

                if(grade.get("student_id").equals(i)) {
                    //colection.remove(grade);
                    System.out.println(grade);
                    i++;
                }
            }
        }finally {
            cursor.close();
        }*/


    }
}
