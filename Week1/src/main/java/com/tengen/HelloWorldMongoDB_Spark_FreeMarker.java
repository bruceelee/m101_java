package com.tengen;

import com.mongodb.*;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

import java.io.IOException;
import java.io.StringWriter;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by STEV on 08-01-2015.
 */
public class HelloWorldMongoDB_Spark_FreeMarker {
    public static void main(String[] args) throws IOException, TemplateException {

        //MongoDB
        MongoClient client= new MongoClient(new ServerAddress("localhost",27017));

        DB database= client.getDB("test");
        DBCollection collection= database.getCollection("gatos");
        DBObject document = collection.findOne();


        //FreeMarker
        Configuration configuration= new Configuration();
        configuration.setClassForTemplateLoading(HelloWorldFreeMarker.class,"/");

        Template helloTemplate = configuration.getTemplate("hello.ftl");
        final StringWriter writer= new StringWriter();
        //Map<String,Object> helloMap= new HashMap<String, Object>();
        //helloMap.put("name", "Freemarker");

        helloTemplate.process(document, writer);
        //System.out.println(writer);

        //Comunicacion con Spark
        Spark.get(new Route("/") {
            @Override
            public Object handle(final Request req, final Response res) {
                return writer;
            }
        });

    }
}
