package com.tengen;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;


/**
 * Created by STEV on 08-01-2015.
 */
public class HelloWorldSparkJava {

    public static void main(String []args){

        Spark.get(new Route("/") {
            @Override
            public Object handle(final Request req, final Response res) {
                return "Hello World from Spark";
            }
        });

    }
}
