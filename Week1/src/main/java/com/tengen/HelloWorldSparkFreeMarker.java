package com.tengen;

import freemarker.template.Configuration;
import freemarker.template.Template;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by STEV on 08-01-2015.
 */
public class HelloWorldSparkFreeMarker {
    public static void main(String[] args) throws Exception{

        //FreeMarker
        Configuration configuration= new Configuration();
        configuration.setClassForTemplateLoading(HelloWorldFreeMarker.class,"/");

        Template helloTemplate = configuration.getTemplate("hello.ftl");
        final StringWriter writer= new StringWriter();
        Map<String,Object> helloMap= new HashMap<String, Object>();
        helloMap.put("name", "Freemarker");

        helloTemplate.process(helloMap, writer);
        //System.out.println(writer);


        //Comunicacion con Spark
        Spark.get(new Route("/") {
            @Override
            public Object handle(final Request req, final Response res) {
                return writer;
            }
        });



    }

}
