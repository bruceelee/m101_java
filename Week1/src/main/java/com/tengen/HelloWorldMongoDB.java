package com.tengen;

import com.mongodb.*;

import java.net.UnknownHostException;

/**
 * Created by STEV on 07-01-2015.
 */
public class HelloWorldMongoDB {

  public static void main(String[] args) throws UnknownHostException {

      MongoClient client= new MongoClient(new ServerAddress("localhost",27017));

      DB database= client.getDB("test");
      DBCollection collection= database.getCollection("gatos");

      DBObject document = collection.findOne();
      System.out.println(document);


  }
}
