##En qué estamos

###Java Driver (http://api.mongodb.org/):

#1. Arrays.asList(), permite agregar un array de String a un BasicDBObject: 
		 
```
#!java

String[] lista= new String[]{"Java","C++"};
		 BasicDBObject doc = new BasicDBObject();
		 doc.put("languages", Arrays.asList(lista));
```


#2. Agregar un objeto dentro de otro objeto:
		
```
#!java

 doc.put("address", new BasicDBObject("street", "20 Main")
                .append("town", "Westfield")
                .append("zip", "56789"));


#3. GridFS: 
Guarda Blobs en dos colecciones, una para la metadata y la otra los chunks de tamańo 16mbs (maximo tamańo que puede medir un documento). Buscar en Week 3 para mas info.


#4. Si quieres obtener una lista de DBObject inicializala como ArraList de DBObject, sino obtendras java.null.pointer.Exception.
	     
```
#!java

public List<DBObject> findByDateDescending(int limit) {

	        List<DBObject> posts = new ArrayList<DBObject>();
	      	DBCursor cursor=  postsCollection.find().sort(new BasicDBObject("date",-1));

	        while(cursor.hasNext()){
	            posts.add(cursor.next());
	        }
	        return posts;
	    }


#5. Index: Existen dos opciones. Generando el objeto de a poco, o bien especificando el nombre del index directamente.
```
#!java
	 BasicDBObject myHint = new BasicDBObject("a", 1).append("b",-1).append("c", 1);

     DBObject doc = c.find(query).hint(myHint).explain();
...
#!java
	 DBObject doc = c.find(query).hint("a_1_b_-1_c_1").explain();

###Mongo Shell:
	0. Sort
		db.grades.find({ score:{$gte:65}}).sort({score:1})
	1. $lt , $gt :  db.scores.find({ score: {$lt:70} }). Puntaje menor a 70, busca el primero.
	
	2. $or:
		db.scores.find({ $or: [ {score: {$lt:70} } , { score: {$gt:200} }  ] })
	2.1 Queries dentro de arrays
		db.products.find( { tags : "shiny" } );
		Posible respuesta: * { _id : 42 , name : "Whizzy Wiz-o-matic", tags : [ "awesome", "shiny" , "green" ] } 
						   * { _id : 1040 , name : "Snappy Snap-o-lux", tags : "shiny" } 
	2.2 Queries con "dot notation"
		Write a query that finds all products that cost more than 10,000 and that have a rating of 5 or better.
			{ product : "Super Duper-o-phonic", 
			  price : 100000000000,
			  reviews : [ { user : "fred", comment : "Great!" , rating : 5 },
			              { user : "tom" , comment : "I agree with Fred, somewhat!" , rating : 4 } ],
			  ... }
		Solucion= db.catalog.find( { price : { $gt : 10000 } , "reviews.rating" : { $gte : 5 } } );
	3. $in, $all: 
		//Busca en cada fila a partir de un ARRAY si se cumple UN valor para name
		db.students.find({ name: { $in:["Esteban","Daniela"] } }) 
		//Busca en cada fila que se cumpla TODAS los valores buscados a partir de un ARRAY 
		db.students.find({ subjects: { $all:["MongoDB", "Angular"]}}) 
		
	4. Update $set, $unset:
		db.students.update({ score:{ $lt:70} }, { $set: { approved: false} } ) //Actualizo solo un campo, si no existo lo crea.
		db.students.update({ score:{ $gt:70} }, {$unset: { approved: 1}} )  //Borro el campo buscado
		
	5. Upsert: Si al hacer update, no existe la fila, la genera.
		//No existe estudiante con 200 anios, se crea uno con ese nombre
		db.students.update({ $age: {$gt:200}}, {type: "inmortal"}, {upsert:true} ) 
		
	6. Multi-update: db.scores.update({BUSQUEDA}, {CAMBIO} , {multi:true})
	
	7. Remove: db.scores.remove({})  	  //Borra todo como si fuera multi:true
			   db.scores.remove({query})  //Borra todo lo que cumpla la query


###Index

	1.Para mejorar velocidad de lectura de la bd, es una excelente practica generar indices.
		db.students.ensureIndex({class:1, student_name:1});
	2.Para encontrar los indices existentes
		db.system.indexes.find();

	  Para listar indices
	  	db.system.getIndexes();

	  Para borrar todos los indices
	  	db.students.dropIndex({'student_id':1})
	 3. Multi-llaves
	 	db.foo.ensureIndex({a:1, b: 1})

	 	Ahora este indice no permitira que se hagan inserciones de documentos donde ambos elementos del indice sean arreglos:
	 		-Invalidos: db.foo.insert({a:[1,2,3],b:[3,4,5] })
	 		-Validos: db.foo.insert({a:1, b:3})
	 				  db.foo.insert({a:[1,2,3], b:5})
	 				  db.foo.insert({a:3 , b:[3,4,5]})
	 4. Se pueden agregar indices a sub-partes de un documento

	 	db.directions.ensureIndex({addresses.phone:1})

	 5. Indices unicos

	 *Crear indices donde la llave debe ser unico. Si no nos interesa que el indice tenga llaves repetidas debemos colocar 'unique' como falso
	 	db.stuff.ensureIndex( {thing:1}, {unique:true})
	 	
	 *Para obtener los indices
	 	db.stuff.getIndexes()

	 *Si queremos borrar el indice
	 	db.stuff.dropIndex({thing:1})

	 6. Borrar duplicados
	 *Si queremos borrar duplicados cuando se crea indices unicos. No obstante, cuando se hace esto se borran todos los documentos que tengan conflicto con el indice creado y no se pueden recuperar de ninguna forma.
	 	
	 	{unique:true, dropDups: true}

	 7. Sparse Index

	 Sparse indexes only contain entries for documents that have the indexed field, even if the index field contains a null value. The index skips over any document that is missing the indexed field. The index is “sparse” because it does not include all documents of a collection. By contrast, non-sparse indexes contain all documents in a collection, storing null values for those documents that do not contain the indexed field.

	 To create a sparse index, use the db.collection.ensureIndex() method with the sparse option set to true. 

	 db.products.ensureIndex( { "size": 1 }, { sparse: true } )

	
	 8. Background Index

	 En modo de produccion es preferible crear indices en modo background dado que no posee writelock, el contra es que es un proceso lento, y solo puede crearse un indice para una db al mismo tiempo por background. Por defecto se hace en foreground: es rapido pero bloquea los writes del sistema completo.

	 		background:true

	 9. Tamanio del Index. Es importante administrar bien el espacio utilizado por el sistema, dada las ventajas de los indices es preferible tener en memoria los indices mas que los datos en sí.

	 Para ver cuanto espacio ocupa nuestro db
	 	db.students.stats()
	 Para ver cuanto espacio ocupa los indices
	 	db.students.totalIndexSize()

	 10. Cardinalidad de los indices

	 *Regular:  1:1
	 *Sparse:   <=documents
	 *Multikey: >documents, por cada combinacion posible debe existir una referencia en indice. Por lo que esto puede consumir mas espacio que la informacion.

   	 11. Operadores $gt, $lt, $ne, $nin no son eficientes para ser utilizados con index, se debe buscar el mejor que se puede utilizar con 'hint'

   	 12.Hinting index

   	 Por defecto cuando se realiza una busqueda, el sistema realiza un ranking cada cierto tiempo para descubrir cual es el mejor indice automaticamente. 

   	 Si no existe una duplicacion de indices, el sistema busca automaticqamente el que mejor se acomode a la busqueda. Si se quiere FORZAR a usar uno se debe usar hint:
   		db.people.find().sort({'title':1}).hint({title:"1"})

     Si se quiere utilizar ninguno de los creados por usuario se debe:
   	 	db.people.find().sort({'title':1}).hint({$natural:1})

	 13. Index de texto
		Ejemplo de documentos:
			-{"_id":1, "words": "dog ring ruby"}
			-{"_id":2, "words": "dog shrub ruby"}
			{"_id":2, "words": "dog moss tree"}
		*Agrego el indice	
		db.sentences.ensureIndex({'words': 'text'})

		*Hago una busqueda en 'words' de texto utilizando el indice. Me devuelve todas las coincidencias
		db.sentences.find({$text: {$search: 'dog'}})

		*Tambien si agregar mas elementos en busqueda, pilla si es que existe alguna coincidencia con alguno de los elementos en busqueda como si de un array se tratase. En este caso encuentra los tres documentos.
		db.sentences.find({$text: {$search: 'ring shrub tree'}})
		
		*Se puede agregar un ranking a partir del textScore. Los que tengan mejor puntaje significa que se parecen mas al campo de busqueda.
		db.sentences.find({ $text:{$search: 'dog moss tree'}},  {score: {$meta:'textScore'}}).sort({score:{$meta: 'textScore'}})

###Profiler: Escribe documentos a system.profile. Se usa para consultas que tardan cierto periodo de tiempo.

	0: Off, nivel por defecto
	1: Log slow queries
	2: Log all queries. Es muy util para realizar debugging

	Es conveniente para moniterear el trafico de la base de datos.

	**Uso en cmd. Quiero logear las slow queries que tarden mas de 2 milisegundos:
		mongod --dbpath DIR --profile 1 --slowms 2

	**Obtener nivel que se esta usando
	db.getProfilingLevel() //Solo el nivel
	db.getProfilingStatus() //Obtengo mas info
	**Cambio nivel
	db.setProfilingLevel(1,4) //Se mantiene nivel y aumento tiempo 4 milisegundos en el ejemplo.

	**Encontrar logs de profile.
	//En el ejemplo se busca por 'ns' (namespace) de nombre tal, y se orderan por 'ts' (timestamp). Se debe estar posicionado en el db que deseamos.
	db.system.profile.find({ns:/school.students/}).sort({ts:1})

	**Desafio:Write the query to look in the system profile collection for all queries that took longer than one second, ordered by timestamp descending.

	Respuesta = db.system.profile.find({millis:{$gt:1000}}).sort({ts:-1})


	###Explain

	Permite determinar que indices se estan utilizando, el tiempo que se tarde, ect.
		db.things.find().explain();


### Geolocalización

##2D
	 'location': [x,y]
	 ensureIndex({'location':'2d'})
	 find.({location:{$near: [ x , y ]}}).limit(20) 

	 El operador near, busca de forma ascedente, lugares cercanos por el location.

##3D=2dsphere
	 
	 Buscar http://geojson.org/geojson-spec.html

	 *Generar indice
	 	ensureIndex({'location': '2dsphere'});
	 *Buscar	
	 	db.stores.find({'location':{
	 		$near:{
	 			$geometry:{
	 				type:"Point",
	 				coordinates: [longitud, latitud]},
	 			$maxDistance: NumDistanceMax
	 			}
	 		}
	 	});

###Mongotop y Mongostat

Permiten monitorear el comportamiento del sistema


###Aggregation
	Permite crear una coleccion nueva a partir de operacion de upset a partir de una coleccion y algún criterio.


  1.Aggregation Pipeline
	$project-reshape - 1:1
	$match-filter-n:1
	$group-aggregate- n:1
	$sort-sort- 1:1
	$skip- skips- n:1
	$limit- limit
	$unwind - normalize- 1:n
		example: tags: ["red", "blue", "green"]
				tags: red
				tags:blue
				tags: green
	$out-output- 1:1
	$redact ??
	$geonear ??

	2. Group y Compound Group

	2.1 Para agrupar documentos con respecto a cierto parametro, se genera una coleccion nueva donde el _id es un objeto con la llave de una categoria en especifico.
	db.products.aggregate([ { $group:{_id: {"$category"}, num_products: {$sum:1} }} ])

	Desglosado:
		db.products.aggregate([
			{$group:{
				_id: { label: "$category"},  //Puedes agregar un label diferente al nombre de la categoria
				num_products: {$sum:1}

					}
			}
		])

	2.2 Compound Group: Una llave puede ser un objeto compuesto como el siguiente, lo unico importante es que la llave sea unica.
		_id: {"manufacter": "$manufacter", "category": "$category"}

	3.Aggregation Expressions

	Todos estos funcionan  para $group

	-$sum: Si quiero sumar los valores numericos de "price".
			db.products.aggregate([
		    {$group:
		     {
			 _id: {
			     "maker":"$manufacturer"
			 },
			 sum_prices:{$sum:"$price"}
		     }
		    }
		])
	-$avg:
			db.products.aggregate([
			    {$group:
			     {
				 _id: {
				     "category":"$category"
				 },
				 avg_price:{$avg:"$price"}
			     }
			    }
			])


	-$min/$max: Devuelve el min o el maximo elemento de una categoria numerica o alfanumerica.

	Estos dos necesitan un sort para que tenga sentido usarlas
	-$first: Devuelve primer elemento 
	-$last: Devuelve ultimo elemento

	Estos funcionan con arrays
	
	-$addtoSet: Agrega elementos a un array ssi es que no existe.
     	db.products.aggregate([
		    {$group:
		     {
			 _id: {
			     "maker":"$manufacturer"
			 },
			 categories:{$addToSet:"$category"}
		     }
		    }
		])

	-$push: Agrega elementos a un array pero no se preocupa de la duplicacion
		db.products.aggregate([
		    {$group:
		     {
			 _id: {
			     "maker":"$manufacturer"
			 },
			 categories:{$push:"$category"}
		     }
		    }
		])

	4. Double Group: Puedes agregar varios groups de forma de generar un group final

	db.grades.aggregate([
	    {'$group':{_id:{class_id:"$class_id", student_id:"$student_id"}, 'average':{"$avg":"$score"}}},
	    {'$group':{_id:"$_id.class_id", 'average':{"$avg":"$average"}}}   //El $_id.class_id, toma de _id el group anterior, y calcula el average 
	    ])

	5. $project viene de projection y permite modificar los documentos de forma 1:1, además permite:
		-Remover llaves
		-Agregar nuevas llaves
		-Modificar llaves
		-Utilizar algunas funciones simples sobre los campos incluso sobre las llaves como:
			$toUpper
			$toLower
			$add
		$multiply

		*Cambio a minusculas un campo. Notese que se elimina _id=0, pero se deja zip= $_id como nueva llave. 
			use agg
				db.zips.aggregate([{$project:{_id:0, city:{$toLower:"$city"}, pop:1, state:1, zip:"$_id"}}])


		*Hacer un reshape, el orden de los campos no necesariamente es el mismo que el indicado.
			use agg
			db.products.aggregate([
			    {$project:
			     {
				 _id:0,
				 'maker': {$toLower:"$manufacturer"},
				 'details': {'category': "$category",
					     'price' : {"$multiply":["$price",10]}
					    },
				 'item':'$name'
			     }
			    }
			])

	6. $match

	Funciona como find, agrupa los documentos que coincidan con el campo de busqueda. 
		db.zips.aggregate([
		    {$match:
		     {
			 state:"NY"
		     }
		    }
		])

	Se adiciona el metodo $group para que el resultado ademas sea agrupado.
		db.zips.aggregate([
		    {$match:
		     {
			 state:"NY"
		     }
		    },
		    {$group:
		     {
			 _id: "$city",
			 population: {$sum:"$pop"},
			 zip_codes: {$addToSet: "$_id"}
		     }
		    }
		])
	
	Recordemos que con el metodo $project podemos proyectar los documentos para cambiarle el nombre de las llaves, o no permitir que algunos campos de los documentos no aparezcan (0=no aparece, 1= si aparece). En este caso se elimina el nombre de la llave, y se deja que city=$_id tenga el valor de la llave.

		db.zips.aggregate([
		    {$match:
		     {
			 state:"NY"
		     }
		    },
		    {$group:
		     {
			 _id: "$city",
			 population: {$sum:"$pop"},
			 zip_codes: {$addToSet: "$_id"}
		     }
		    },
		    {$project:
		     {
			 _id: 0,
			 city: "$_id",
			 population: 1,
			 zip_codes:1
		     }
		    }
		     
		])

	7. $sort: Se puede realizar antes o despues de grouping.

	db.zips.aggregate([
	    {$sort:
	    {
	    state:1,
	    city:1
	    }
	 }
	])


	7.1 Metodos utiles con $sort: Estos metodos tienen mas sentido si es que esta ordenado $sort.

	7.1.1 $skip - $limit: 

	 $skip se salta documentos según los requeridos y continua desde ahi. $limit hace que solo se traigan la cantidad de documentos indicados.

	 db.zips.aggregate([
	    {$match:
	     {
		 state:"NY"
	     }
	    },
	    {$group:
	     {
		 _id: "$city",
		 population: {$sum:"$pop"},
	     }
	    },
	    {$project:
	     {
		 _id: 0,
		 city: "$_id",
		 population: 1,
	     }
	    },
	    {$sort:
	     {
		 population:-1
	     }
	    },
	    {$skip: 10},
	    {$limit: 5}
	])



	7.1.2  $first - $last


	db.zips.aggregate([
	    /* get the population of every city in every state */
	    {$group:
	     {
	     _id: {state:"$state", city:"$city"},
	     population: {$sum:"$pop"},
	     }
	    },
	     /* sort by state, population */
	    {$sort: 
	     {"_id.state":1, "population":-1}
	    },
	    /* group by state, get the first item in each group */
	    {$group: 
	     {
	     _id:"$_id.state",
	     city: {$first: "$_id.city"},
	     population: {$first:"$population"}
	     }
	    }
	])
	

	8. $unwind: Separa un array haciendo un unjoin y haciendo un group con join de los elementos del array con el resto de los elementos del documento. 

		{ a:1, b:1 , c:['hard', 'shiny', 'pointy', 'thin']}

		Luego de $unwind: "$c" quedaria:

		{a:1, b:1, c: 'hard'}
		{a:1, b:1, c: 'shiny'}
		{a:1, b:1, c: 'pointy'}
		{a:1, b:1, c: 'thin'}

		Un ejemplo un poco mas elaborado. Deseo unwind los tags y agruparlos con el objetivo de contarlos. Luego de esto los ordeno por populadirad.

		db.posts.aggregate([
		    /* unwind by tags */
		    	{"$unwind":"$tags"},
		    /* now group by tags, counting each tag */
			    {"$group": 
			     {"_id":"$tags",
			      "count":{$sum:1}
			     }
		    	},
		   	 /* sort by popularity */
		    	{"$sort":{"count":-1}},
		    /* show me the top 10 */
		   		{"$limit": 10},
		    /* change the name of _id to be tag */
			    {"$project":
			     {_id:0,
			      'tag':'$_id',
			      'count' : 1
			     }
			    }
		    ])

		NOTA: Se puede deshacer el proceso de $unwind con $push.


	8.1 Double $unwind

	db.inventory.drop();
	db.inventory.insert({'name':"Polo Shirt", 'sizes':["Small", "Medium", "Large"], 'colors':['navy', 'white', 'orange', 'red']})
	db.inventory.insert({'name':"T-Shirt", 'sizes':["Small", "Medium", "Large", "X-Large"], 'colors':['navy', "black",  'orange', 'red']})
	db.inventory.insert({'name':"Chino Pants", 'sizes':["32x32", "31x30", "36x32"], 'colors':['navy', 'white', 'orange', 'violet']})
	db.inventory.aggregate([
	    {$unwind: "$sizes"},
	    {$unwind: "$colors"},
	    {$group: 
	     {
		'_id': {'size':'$sizes', 'color':'$colors'},
		'count' : {'$sum':1}
	     }
	    }
	])

	NOTA: Se puede deshacer el proceso con un double $push

	db.inventory.drop();
	db.inventory.insert({'name':"Polo Shirt", 'sizes':["Small", "Medium", "Large"], 'colors':['navy', 'white', 'orange', 'red']})
	db.inventory.insert({'name':"T-Shirt", 'sizes':["Small", "Medium", "Large", "X-Large"], 'colors':['navy', "black",  'orange', 'red']})
	db.inventory.insert({'name':"Chino Pants", 'sizes':["32x32", "31x30", "36x32"], 'colors':['navy', 'white', 'orange', 'violet']})
	db.inventory.aggregate([
	    {$unwind: "$sizes"},
	    {$unwind: "$colors"},
	    /* create the color array */
	    {$group: 
	     {
		'_id': {name:"$name",size:"$sizes"},
		 'colors': {$push: "$colors"},
	     }
	    },
	    /* create the size array */
	    {$group: 
	     {
		'_id': {'name':"$_id.name",
			'colors' : "$colors"},
		 'sizes': {$push: "$_id.size"}
	     }
	    },
	    /* reshape for beauty */
	    {$project: 
	     {
		 _id:0,
		 "name":"$_id.name",
		 "sizes":1,
		 "colors": "$_id.colors"
	     }
	    }
	])


	9. Limitaciones de Pipeline

	- Existe un limite de 100mb para Pipeline en Disk memory.
	- 16mb limit por documento por defecto. 
	- Sharded: Cuando una aplicacion basada en MongoDB tiene muchos BDs en lugares diferentes, y cuando manda a actualizar por Group by ó sort, esto puede producir una sobrecarga en el sistema o algo asi. En este caso podria ser bueno usar Hadoop   